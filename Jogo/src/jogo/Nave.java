package jogo;

import jogoutil.PlayWave;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;

/**
 *
 * @author johna
 */
class Nave extends Ator {
    private static final PlayWave astroSound = 
                                        new PlayWave("res/sons/astronauta.wav");
    private double vida = 100.0;
    @Override
    public void move(double dt) {
        super.move(dt);
        if(getX() < 1) setX(1);
        if(getX() > BOUND - getLargura()) setX(BOUND - getLargura());        
        if(getY() < 1) setY(1);
        if(getY() > BOUNDY - getAltura()) setY(BOUNDY - getAltura());        
    }

    private final List<Missil> misseis;
    private boolean canFire=true;
    
    public Nave(Fase f){
        super(new ImageIcon("res/imagens/naves/1.gif"), 100.0, 100.0, 0.0, 0.0);
        misseis = new LinkedList<>();
    }
    
    public List<Missil> getMisseis() {
        return misseis;
    }
    
    private void adicionaMissil(double x, double y){
        Missil m = new Missil(x,y,getVx(),getVy());
        this.misseis.add(m);
    }

    public void atira(){
        if (canFire) {
            PlayWave.play("res/sons/laser.wav");
            canFire = false;
            adicionaMissil(getX()+super.getLargura(), getY());
            adicionaMissil(getX()+super.getLargura(), getY()+super.getAltura()*5/6);
        }
    }
    
    public void atiraBack(){
        if (canFire) {
            PlayWave.play("res/sons/laser.wav");
            canFire = false;
            Missil m = new Missil(getX(),getY() + getAltura()/2,getVx(),getVy());
            this.misseis.add(m);
            m.setVx(-15.0);
        }
    }
    
    public void setVelocidade(int codigo, double velocidade){
        if(codigo == KeyEvent.VK_UP) setVy(-velocidade);
        if(codigo == KeyEvent.VK_DOWN) setVy(velocidade);
        if(codigo == KeyEvent.VK_LEFT) setVx(-velocidade);
        if(codigo == KeyEvent.VK_RIGHT) setVx(velocidade);
    }

    public void keyPressed(KeyEvent tecla){     
        int codigo = tecla.getKeyCode();        
        if(codigo == KeyEvent.VK_SPACE) atira();
        if(codigo == KeyEvent.VK_B) atiraBack();
        setVelocidade(codigo,10.0);
    }
    
    public void keyReleased(KeyEvent tecla){        
        int codigo = tecla.getKeyCode();        
        if(codigo == KeyEvent.VK_SPACE || codigo == KeyEvent.VK_B) 
            canFire = true;
        setVelocidade(codigo,0.0);
    }
    
    public void winnerSound() {
        astroSound.setGain(10);
        astroSound.play();
    }
    
    public double maxVelModule() { return 30.0; }

    @Override
    public void colisao(Corpo that) {
        SemRumo in = (SemRumo) that;
        if (in.isLixo()) {
            setVx(getVx()/5);
            setVy(getVy()/5);            
            double razao = 3*getMassa();
            vida = vida - (100*that.getMassa()/razao);
        }
        else { 
            vida = vida + 50;
            if (vida > 100) revive();
            in.setVisivel(false);
            winnerSound();
        }
    }
    public double getVida() { return vida; }
    public void revive() { vida = 100.0; }
}
