package jogo;

import edu.princeton.cs.algs4.StdRandom;

public class Corpo {
    private double x, y;
    private double vx, vy;
    private final double raio;
    private final double massa;
    private int colisoes;
    protected static final int BOUND=GerenciadorDeTelas.LARGURA_DA_TELA;
    protected static final int BOUNDY=GerenciadorDeTelas.LARGURA_DA_TELA - 70;
    
    public Corpo(double x, double y, double w, double h) {
    	this(Double.min(w, h)*2/5, x, y);
    }
    public Corpo(double x, double y, double w, double h, double vx, double vy) {
    	this(Double.min(w, h)*2/5, x, y, vx, vy);
    }
    public Corpo(double raio){
    	this(raio, -1.0, -1.0);
    }

    public double maxVelModule() { return 15.0; }
    public Corpo() {
        this(StdRandom.uniform(15,60));
    }
    public Corpo(double raio, double vx, double vy) {
    	this(raio, StdRandom.uniform(BOUND), StdRandom.uniform(BOUNDY), vx, vy);
    }   
    public Corpo(double raio, double x, double y, double vx, double vy) {
        this.raio = raio;
        this.x = x;
        this.y = y;        
        massa = raio*raio*3.14;
        colisoes = 0;
        if (vx == -1.0) vx = getRandomVel();
        this.vx = vx;
        if (vy == -1.0) vy = getRandomVel();
        this.vy = vy; 
        this.vx=getBound(this.vx);
        this.vy=getBound(this.vy);        
    }
    private double getRandomVel() {
    	double v = StdRandom.uniform(0.5, 3);
    	if (StdRandom.uniform(2) == 0) v = -v;
    	return v;
    }    
    private void colidiu() {
    	if (colisoes >= 0) ++ colisoes;
    }
    @Override
    public String toString() {		
            return super.toString() + ": x=" + x + ": y=" + y + ": vx=" + 
                      vx + ": vy=" + vy + ": r=" + raio + ": c=" + colisoes;
    }
    public double getX() { return x; }
    public double getY() { return y; }
    public void invalida() {
    	colisoes = -1;
    }
    public int getColisoes() {
        return colisoes;
    } 
    private double getBound(double v) {
        final double maxVModule = maxVelModule();
        final double minVModule = 0.2;
        if (v>=0 && v<minVModule) v = minVModule;
        if (v<=0 && v>-minVModule) v = -minVModule;
        if (v>maxVModule) v = maxVModule;
        if (v<-maxVModule) v = -maxVModule;
        return v;
    }
    private boolean isBound(double p, double limit) {
        double lower = p - raio;
        double upper = p + raio;
        return ((Double.compare(lower, 0)>=0) && 
                (Double.compare(upper, limit)<=0));
    }
    private boolean isBound() {
        return isBound(x, BOUND) && isBound(y,BOUNDY);
    }
    public void move( double dt) {
    	assert dt >= 0.0;
        boolean wasBoundedX = isBound(x, BOUND);
        boolean wasBoundedY = isBound(y, BOUNDY);
  	x = x + vx*dt;
        y = y + vy*dt;
        if (wasBoundedX){
            if (!isBound(x, BOUND)) atingeLimiteVertical();            
        }
        if (wasBoundedY) {
            if (!isBound(y, BOUNDY)) atingeLimiteHorizontal();
        }
        
    }    
    public double timeToHit(Corpo b) {        
    	Corpo a = this;
        if (a == b) return Double.POSITIVE_INFINITY;

        double dx  = b.x - a.x;
        double dy  = b.y - a.y;
        double dvx = b.vx - a.vx;
        double dvy = b.vy - a.vy;
        double dvdr = dx*dvx + dy*dvy;
        if (dvdr > 0) return Double.POSITIVE_INFINITY;
        double dvdv = dvx*dvx + dvy*dvy;
        double drdr = dx*dx + dy*dy;
        double sigma = a.raio + b.raio;
        double d = (dvdr*dvdr) - dvdv * (drdr - sigma*sigma);
        if (d < 0) return Double.POSITIVE_INFINITY;
        double toHit = -(dvdr + Math.sqrt(d)) / dvdv;
        double xHit = x + vx*toHit;
        if (Double.compare(xHit, 0)<0) return Double.POSITIVE_INFINITY;
        if (Double.compare(xHit, BOUND)>0) return Double.POSITIVE_INFINITY;
        double yHit = y + vx*toHit;
        if (Double.compare(yHit, 0)<0) return Double.POSITIVE_INFINITY;
        if (Double.compare(yHit, BOUNDY)>0) return Double.POSITIVE_INFINITY;
        return toHit;
    }    

    private double timeToHitLimiteVerticalInterno() {        
        if (vx > 0) return (BOUND-x-raio)/vx;
        else if (vx < 0) return -(x-raio)/vx;
        else return Double.POSITIVE_INFINITY;
    }
    public double timeToHitLimiteVertical() { 
        double t = timeToHitLimiteVerticalInterno();
        if (t < 0) t = 0.0;
        return t;
    }

    private double timeToHitLimiteHorizontalInterno() {
        if (vy > 0) return (BOUNDY-y-raio)/vy;
        else if (vy < 0) return -(y-raio)/vy;
        else return Double.POSITIVE_INFINITY;
    }
    public double timeToHitLimiteHorizontal() { 
        double t = timeToHitLimiteHorizontalInterno();
        if (t < 0) t = 0.0;
        return t;
    }    
    public void colisao(Corpo that, double massaPraUsar) {
        double dx = that.x - this.x, dy = that.y - this.y;
        double dvx = that.vx - this.vx, dvy = that.vy - this.vy;
        double dvdr = dx*dvx + dy*dvy;
        double dist = this.raio + that.raio;
        double J = 2 * massaPraUsar * that.massa * dvdr / 
                                             ((this.massa + that.massa)* dist);
        double Jx = J * dx / dist;
        double Jy = J * dy / dist;
        this.vx += Jx / massaPraUsar;
        this.vy += Jy / massaPraUsar;
        that.vx -= Jx / that.massa;
        that.vy -= Jy / that.massa;
        
        vx=getBound(vx);
        vy=getBound(vy);
        colidiu();
        that.colidiu();
    }    
    public void colisao(Corpo that) {
    	colisao(that, this.massa);
    }    
    public void atingeLimiteVertical() {
        vx = -vx;        
        colidiu();
    }    
    public void atingeLimiteHorizontal() {
        vy = -vy;
        colidiu();
    }
    public void setVx(double vx){
    	this.vx = vx;
        this.vx=getBound(this.vx);
    	colidiu();
    }
    public void setVy(double vy){
    	this.vy = vy;
        this.vy=getBound(this.vy);
    	colidiu();
    }
    public double getVx(){
    	return this.vx;
    }
    public double getVy() {
    	return this.vy;
    }
    public void setX(double x) {
    	this.x = x;
    }
    public void setY(double y) {
    	this.y = y;
    }
    public double getMassa() { return massa; }
}
