package jogo;

import javax.swing.ImageIcon;

public class Missil extends Ator {
    private final static ImageIcon MISSIL_IMG = 
                                       new ImageIcon("res/imagens/missil3.gif");
	

	
    @Override
    @SuppressWarnings("unchecked")
    public void colisao(Corpo that) {
            if (that.getClass() == SemRumo.class) {
                    setVisivel(false);
                    ((SemRumo)that).setVisivel(false);
            }
    }

    @Override
    public void atingeLimiteVertical() {
        setVisivel(false);
    }

    @Override
    public void atingeLimiteHorizontal() {
        setVisivel(false);
    }
    
    public double maxVelModule() { return 30.0; }

    public Missil(double x, double y, double vx, double vy){
            super(MISSIL_IMG, x, y, Double.max(30.0, 10.0+vx), vy/5.0);
    }
}
