/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo;

import java.util.Objects;

/**
 *
 * @author johna
 */
public class Colisao implements Comparable<Colisao> {

    public Corpo getA() {
        return a;
    }

    public Corpo getB() {
        return b;
    }

    public double getTempoDaColisao() {
        return tempoDaColisao;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int)
                (Double.doubleToLongBits(this.tempoDaColisao) ^
                (Double.doubleToLongBits(this.tempoDaColisao) >>> 32));
        hash = 79 * hash + Objects.hashCode(this.a);
        hash = 79 * hash + Objects.hashCode(this.b);
        hash = 79 * hash + this.countA;
        hash = 79 * hash + this.countB;
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Colisao other = (Colisao) obj;
        if (Double.doubleToLongBits(this.tempoDaColisao) !=
                            Double.doubleToLongBits(other.tempoDaColisao)) {
            return false;
        }
        if (this.countA != other.countA) {
            return false;
        }
        if (this.countB != other.countB) {
            return false;
        }
        if (!Objects.equals(this.a, other.a)) {
            return false;
        }
        if (!Objects.equals(this.b, other.b)) {
            return false;
        }
        return true;
    }
    private final double tempoDaColisao;
    private final Corpo a, b;
    private final int countA, countB;
    @Override
    public String toString() {
        String rc = super.toString();
        if (null != a) rc += ", "  + a.toString();
        else rc += ", " + "null";
        if (null != b) rc += ", " + b.toString();
        else rc += ", " + "null";
        return rc;
    }

    public Colisao(double t, Corpo a, Corpo b) {
        tempoDaColisao = t;
        assert tempoDaColisao > 0.0;
        this.a = a;
        this.b = b;
        countA = getCount(a);
        countB = getCount(b);
    }
    @Override
    public int compareTo(Colisao that) {
        if (this.tempoDaColisao < that.tempoDaColisao) return -1;
        else if (this.tempoDaColisao > that.tempoDaColisao) return 1;
        else return 0;
    }
    public boolean isValid() {
        int newCountA = getCount(a);
        int newCountB = getCount(b);
        return ( (newCountA != -1) && (newCountB != -1) &&
                 (countA == newCountA) && (countB == newCountB));
    }
    private int getCount(Corpo p) {
        if (null == p) return 0;
        return p.getColisoes();
    }
        
}
