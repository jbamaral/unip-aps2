package jogo;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

abstract class Ator extends Corpo {
    private Image imagem;
    private boolean isVisivel;
    private double altura;
    private double largura;
    Rectangle rect;

    Ator(ImageIcon imagem, double x, double y) {
        this(imagem, x, y, -1.0, -1.0);
    }
    Ator(ImageIcon imagem, double x, double y, double vx, double vy) {
        super(x, y, imagem.getImage().getWidth(null), imagem.getImage().getHeight(null), vx, vy);
        this.imagem = imagem.getImage();
        largura = this.imagem.getWidth(null);
        altura = this.imagem.getHeight(null);
        isVisivel = true;
        rect = new Rectangle((int)this.getX(),(int)this.getY(), (int)largura, (int)altura);
    }
    public Image getImagem() {
        return imagem;
    }
    public boolean isVisivel() {
        return isVisivel;
    }
    public void setVisivel(boolean isVisivel) {
        this.isVisivel = isVisivel;
        if (!isVisivel) 
            invalida();
    }
    @Override
    public double getX() {
        return super.getX() - largura/2;
    }
    @Override
    public double getY() {
        return super.getY() - altura/2;
    }
    @Override
    public void setX(double x) {
        super.setX(x+largura/2);
    }
    @Override
    public void setY(double y) {
        super.setY(y + altura/2);
    }
    public double getLargura() {
        return largura;
    }
    public double getAltura() {
        return altura;
    }
    public boolean intersects(Ator that) {
        rect.setLocation((int)getX(), (int)getY());
        that.rect.setLocation((int)that.getX(), (int)that.getY());
        return rect.intersects(that.rect);
    }
}
