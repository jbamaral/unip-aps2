package jogo;

import jogoutil.MidiPlayer;
import jogoutil.PlayWave;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.StdRandom;
import java.io.File;

public class Fase extends JPanel implements ActionListener {
    private static final long serialVersionUID = 582637237620067493L;
    private Image fundo;
    private Nave nave;
    private final Timer timer;
    private boolean emJogo;
    private List<SemRumo> semRumo;
    private MinPQ<Colisao> colisoes;
    private double tempoDaFase = 0.0;
    private int fase = 1;
    private final ImageIcon fimJogo;
    private final ImageIcon inicio;
    private final File[] mensagensFimDeFase;
    private final File[] fundosFases;
    private ImageIcon correnteParado;
    private int score;
    private int imunidade = 1;
    private final PlayWave bkgSound;
    private List<Explosao> explosoes;
    private static final PlayWave SOM_EXPL =
                                          new PlayWave("res/sons/explosao.wav");
    private static final PlayWave ASTRO_MORRE =
                                             new PlayWave("res/sons/morre.wav");
    private static final PlayWave faseSound = 
                                              new PlayWave("res/sons/fase.wav");
    Colisao lastColision = null;

    public Fase() {
        File fundosFasesFolder = new File("res/imagens/fundos/fases/");
        fundosFases = fundosFasesFolder.listFiles();
        ImageIcon referencia = new ImageIcon(fundosFases[0].getPath());
        fundo = referencia.getImage();
        nave = new Nave(this);
        explosoes = null;
        emJogo = false;
        timer = new Timer(30, this);
        timer.start();
        fimJogo = new ImageIcon("res/imagens/fundos/jogo.png");
        File mensagensFasesFolder = new File("res/imagens/fundos/msg/");
        mensagensFimDeFase = mensagensFasesFolder.listFiles();

        inicio = new ImageIcon("res/imagens/fundos/inicio1.png");
        bkgSound = new PlayWave("res/sons/niveis/level-6-section-1.wav");
        correnteParado = inicio;
    }
    public void initFase() {
        setDoubleBuffered(true);
        setFocusable(true);
        addKeyListener(new TecladoAdapter());
    }
    public void inicializaSemRumo() {
        explosoes = new LinkedList<>();
        semRumo = new LinkedList<>();
        colisoes = new MinPQ<>();
        nave = new Nave(this);
        int createSeed = fase;
        if (createSeed > 10) createSeed = 10;
        int numSemRumo = StdRandom.uniform(2*createSeed+5,3*createSeed+15);
        for (int i = 0; i < numSemRumo; i++) {
            SemRumo a = new SemRumo(fase);
            semRumo.add(a);            
        }
        for (SemRumo sr: semRumo) preveColisao(sr);
    }

    private void drawActor(Graphics2D g, Ator a) {
        g.drawImage(a.getImagem(), (int)a.getX(), (int)a.getY(), this);
    }
    private void drawActorList(Graphics2D g, List<?> l) {
        for  (@SuppressWarnings("unchecked")
        Iterator<Ator> i = (Iterator<Ator>) l.iterator(); i.hasNext(); ) {
            Ator a = i.next();
            if (a.isVisivel()) drawActor(g, a);
            else if (a.getClass() == Explosao.class) i.remove();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void paint(Graphics g) {
        long start = System.nanoTime();
        Graphics2D graficos = (Graphics2D) g;

        graficos.drawImage(fundo, 0, 0, fundo.getWidth(null),
                                                    fundo.getHeight(null),null);

        if (emJogo) {
            List<Missil> misseis = nave.getMisseis();
            drawActorList(graficos, misseis);

            drawActorList(graficos, semRumo);

            drawActor(graficos, nave);

            drawActorList(graficos, explosoes);

            graficos.setColor(Color.white);
            graficos.drawString("FASE " + fase, 5, 15);
            graficos.drawString("VIDA RESTANTE: " +
                                  String.format("%.2f", nave.getVida()), 5, 30);
            graficos.drawString("LIXOS RESTANTES: " + semRumo.size(), 5, 45);
            graficos.drawString("PLACAR: " + score, 5, 60);


        }
        else {
            graficos.drawImage(correnteParado.getImage(), 0, 0, getWidth(),
                                                              getHeight(),null);
        }
        g.dispose();
        long delta = (System.nanoTime() - start)/1000000;
        if (delta > 30) {
            System.out.println("Took " + delta);
        }
    }
    @Override
    public void actionPerformed(ActionEvent arg0) {
        long start = System.nanoTime();

        if (emJogo && null != semRumo) {
            if (semRumo.isEmpty()) {
                emJogo = false;
                correnteParado = new ImageIcon(mensagensFimDeFase[
                       StdRandom.uniform(mensagensFimDeFase.length)].getPath());
                fase++;
                faseSound.setGain(10);
                faseSound.play();
                bkgSound.stop();
            }
            else checarColisoes();
        }
        repaint();
        long delta = (System.nanoTime() - start)/1000000;
        if (delta > 30) {
            System.out.println("Took " + delta + " to check ");
        }

    }
    public void adicionaCorpo(Corpo c) {
        preveColisao(c);
    }
    private boolean checkAndMove(List<?> l, double dt){
        boolean hasVisible = false;
        for  (@SuppressWarnings("unchecked")
        Iterator<Ator> i = (Iterator<Ator>) l.iterator(); i.hasNext(); ) {
            Ator a = i.next();
            int numColisions = a.getColisoes();
            if (a.isVisivel()) {
                a.move(dt);
                boolean isVisible = ((a.getX() + a.getLargura()/2) < 
                                                                    Ator.BOUND);
                if (isVisible) hasVisible = true;
            }
            else i.remove();
            if (a.isVisivel() && numColisions < a.getColisoes()) 
                preveColisao(a);
        }
        return hasVisible;
    }
    private void checkSpaceShipColisions() {
        if (imunidade > 0) ++imunidade;
        else {
            for (SemRumo sr: semRumo) {
                if (nave.intersects(sr)) {
                    nave.colisao(sr);
                    if (sr.isLixo()) {
                        explode( (nave.getX()+nave.getLargura()/2+sr.getX())/2,
                                (nave.getY()+nave.getAltura()/2 + sr.getY())/2);
                    }
                    sr.setVisivel(false);
                    imunidade = 20;
                }
            }
            if (nave.getVida() <= 0) {
                emJogo = false;
                correnteParado = fimJogo;
                MidiPlayer.stop();
            }
        }
    }
    private void moveActors(double dt) {
        if (Double.compare(dt,0)>0) {            
            if (!checkAndMove(semRumo, dt)) {
                for (SemRumo sr: semRumo) {
                    if (sr.getX() > Ator.BOUND) {
                        if (Double.compare(sr.getVx(), 0) < 0 ) {
                            sr.setX(Ator.BOUND - sr.getLargura()/2);
                            break;
                        }
                    }
                }
            }
            checkAndMove(nave.getMisseis(), dt);
        }
    }
    private void checkSemRumoColisions() {
        double dt = 0.7;
        double nextTempoDaFase = tempoDaFase + dt;
        while (!colisoes.isEmpty()) {
            if (null == lastColision) lastColision = colisoes.delMin();
            if (lastColision.isValid()) {
                if (nextTempoDaFase > lastColision.getTempoDaColisao()) {
                    Corpo a = lastColision.getA();
                    Corpo b = lastColision.getB();
                    double dtColisao;
                    dtColisao = lastColision.getTempoDaColisao() - tempoDaFase;
                    tempoDaFase = lastColision.getTempoDaColisao();

                    moveActors(dtColisao);

                    if (a != null && b != null) {
                        a.colisao(b);
                    }
                    preveColisao(a);
                    preveColisao(b);
                    lastColision = null;
                }
                else break;
            }
            else lastColision = null;
            
        }
        moveActors(dt);
        nave.move(0.7);
        tempoDaFase = nextTempoDaFase;
    }
    private void explode(double x, double y) {
        Explosao expl = new Explosao(x,y);
        explosoes.add(expl);
        expl.setVisivel(true);
        SOM_EXPL.setGain(4);                        
        SOM_EXPL.play();

    }
    private void checkMissilColisions() {
        List<Missil> misseis = nave.getMisseis();
        misseis.stream().forEach((m) -> {
            for (SemRumo in: semRumo) {
                if (in.isVisivel() && m.isVisivel() && m.intersects(in)) {
                    if (in.isLixo()) {
                        explode((in.getX()+in.getLargura()/2+m.getX())/2,
                                (in.getY()+in.getAltura()/2 + m.getY())/2);
                        m.colisao(in);
                        score += in.getMassa();
                    }
                    else {
                        score -= 15000;
                        ASTRO_MORRE.setGain(100);
                        ASTRO_MORRE.play();
                    }
                    in.setVisivel(false);
                    m.setVisivel(false);
                }
            }
        });
    }
    private void checarColisoes() {
        checkMissilColisions();
        checkSemRumoColisions();
        checkSpaceShipColisions();
    }
    private class TecladoAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            if (!emJogo) {
                if(e.getKeyChar() == KeyEvent.VK_ENTER){                    
                    if (nave.getVida() < 0) {                        
                        fase = 1;
                        score = 0;
                        nave.revive();
                    }
                    inicializaSemRumo();
                    ImageIcon nextFundo = new ImageIcon(
                            fundosFases[(fase-1)%fundosFases.length].getPath());
                    fundo = nextFundo.getImage();
                    bkgSound.loop();
                    //MidiPlayer.play("res/sons/niveis/level-6-section-1.mid");
                    emJogo = true;
                }
            }
            else nave.keyPressed(e);
        }
        @Override
        public void keyReleased(KeyEvent e) {
            if (emJogo) nave.keyReleased(e);
        }
    }
    private void preveColisao(Corpo o) {        
        if (o == null) return;
        if (o.getClass() != SemRumo.class) return;
        SemRumo a = (SemRumo) o;
        if (a.isVisivel()) {
            double dt;
            for (SemRumo c: semRumo) {        
                if (c.isVisivel() && !a.intersects(c)) {
                    dt = a.timeToHit(c);
                    if (Double.compare(dt, 0) > 0 && !Double.isInfinite(dt))
                        System.out.println("tf = "+ tempoDaFase+", dt = " + dt);
                        colisoes.insert(new Colisao(tempoDaFase+dt, a, c));
                }
            }
        }
    }
}