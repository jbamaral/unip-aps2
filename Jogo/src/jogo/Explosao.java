package jogo;

import java.awt.Image;
import javax.swing.ImageIcon;
import jogoutil.GifDecoder;

public class Explosao extends Ator {
    GifDecoder d;
    int currentFrame;
    int frameRate=2;
    private static final String FILE_PATH="res/imagens/explosoes/explosao.gif";
    private static final ImageIcon EXPLOSAO_IMG = new ImageIcon(FILE_PATH);
    public Explosao(double x, double y) {
        super(EXPLOSAO_IMG, x, y, 0.0, 0.0);
        currentFrame = 0;
        setVisivel(false);
        createImage();        
    }
    private void createImage() {
        d = new GifDecoder();
        d.read(FILE_PATH);
    }
    @Override
    public Image getImagem() {
        int frame = currentFrame/frameRate;
        Image rc = d.getFrame(frame);
        if (frame >= d.getFrameCount()) {
            currentFrame = 0;
            setVisivel(false);
        }
        else {
            ++currentFrame;
        }
        return rc;
    } 
    @Override
    public final void setVisivel(boolean isVisivel) {
        if (true == isVisivel) currentFrame = 0;
        super.setVisivel(isVisivel); 
    }    
}
