package jogo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class GerenciadorDeTelas extends JFrame{  
    private static final long serialVersionUID = 5543760426605797099L;
    public static final int LARGURA_DA_TELA = 600;
    JMenu menuJogo, menuOpcoes, menuHelp;
    JMenuItem itemNovo, itemSair, itemInfo, itemHelp;

    private GerenciadorDeTelas() {  
    }
    
    private void initGame() {
        Fase f = new Fase();
        f.initFase();
        add(f);
        setTitle("Jogo em Java");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setSize(LARGURA_DA_TELA,LARGURA_DA_TELA);
        setBounds(0, 0, LARGURA_DA_TELA, LARGURA_DA_TELA);
        setLocationRelativeTo(null); 
        setResizable(false);
        setJMenuBar(geraBarraMenu());
        setVisible(true);
    }
    
    private JMenuBar geraBarraMenu(){
        JMenuBar barraMenu = new JMenuBar();
        menuJogo = new JMenu("Jogo");
        menuOpcoes = new JMenu("Opcoes");
        menuHelp = new JMenu("Help");
        barraMenu.add(menuJogo);
        barraMenu.add(menuOpcoes);
        barraMenu.add(menuHelp);
        itemHelp = new JMenuItem ("Instruções");
        itemHelp.addActionListener((ActionEvent e) -> {
            JOptionPane.showMessageDialog(null,
                    "- APERTE ''ESPAÇO'' PARA ATIRAR PARA FRENTE \n " +
                            "-APERTE ''B'' PARA ATIRAR PARA TRÁS \n " +
                            "-APERTE ''SETAS'' PARA SE MOVER",
                    "HELP", JOptionPane.INFORMATION_MESSAGE);
        });
        itemNovo =  new JMenuItem("Novo Jogo");
        itemSair = new JMenuItem("Sair");
        itemSair.addActionListener((ActionEvent e) -> {
                    System.exit(0);
                });
        itemInfo = new JMenuItem ("Info");
        itemInfo.addActionListener((ActionEvent e) -> {
                    JOptionPane.showMessageDialog(null,
                            "APS - 4o Semestre - Ciencia da Computacao",
                            "Informacoes", JOptionPane.INFORMATION_MESSAGE);
                });
        menuJogo.add(itemNovo);
        menuJogo.add(itemSair);
        menuOpcoes.add(itemInfo);
        menuHelp.add(itemHelp);
        return barraMenu;       
    }
    

    public static void main(String[] args) {
            GerenciadorDeTelas g = new GerenciadorDeTelas();
            g.initGame();
    }
}
