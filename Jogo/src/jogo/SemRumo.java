package jogo;

import javax.swing.ImageIcon;

import edu.princeton.cs.algs4.StdRandom;

public class SemRumo extends Ator {	
        private static final String BASE_PATH = "res/imagens/inimigos/lixo";
        private static final ImageIcon LIXO_1 = 
                                             new ImageIcon(BASE_PATH + "1.gif");
        private static final ImageIcon LIXO_2 = 
                                             new ImageIcon(BASE_PATH + "2.gif");
        private static final ImageIcon LIXO_3 = 
                                             new ImageIcon(BASE_PATH + "3.gif");
        private static final ImageIcon ASTRONAUTA = 
                                             new ImageIcon(BASE_PATH + "4.gif");
        private static final ImageIcon[] IMAGES = {LIXO_1, LIXO_2, LIXO_3, 
                                                                    ASTRONAUTA};
        private final boolean isLixo;
	
        @SuppressWarnings("OverridableMethodCallInConstructor")
	public SemRumo(double fase){		
		super( newImage(), 
			   BOUND+StdRandom.uniform(50,2000), 
			   StdRandom.uniform(50,(double)BOUNDY-100), 
			   -1.0 - StdRandom.uniform(0.25,4.0)*(fase),
			   StdRandom.uniform(-3.0,3.0)*Double.min(5, fase/5) );
                if (ASTRONAUTA.getImage() == getImagem()) 
                    isLixo = false;
                else 
                    isLixo = true;
	}
	private static ImageIcon newImage() { 
            
            int lixoImg = StdRandom.uniform(100);
            if (lixoImg < 40) lixoImg = 0;
            else if (lixoImg <65) lixoImg = 1;
            else if (lixoImg <90) lixoImg = 2;
            else lixoImg = 3;    
            return IMAGES[lixoImg];
	}
        public boolean isLixo() {
            return isLixo;
        }
        public void colisao(Corpo that) {
            if (that.getClass() == SemRumo.class) {
                SemRumo o  = (SemRumo) that;
                if (this.intersects(o)) super.colisao(that);
            }
            else super.colisao(that);
        }
        public double maxVelModule() { return 15.0; }
}
