package jogoutil;

import java.io.File;
import java.io.IOException;
import javax.sound.midi.InvalidMidiDataException;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;

public class MidiPlayer {
    static PlayingFile p = null;
    static private class PlayingFile {
        private final Thread t1;
        private boolean isPlaying;
        @SuppressWarnings("SleepWhileInLoop")
        private PlayingFile(String file) {
            isPlaying = true;
            t1 = new Thread(() -> {
                Sequencer sequencer=null;
                try {
                    while (isPlaying) {
                        File f = new File(file);
                        sequencer = MidiSystem.getSequencer();
                        Synthesizer synthesizer;
                        MidiChannel[] channels;
                        if (sequencer instanceof Synthesizer) {
                            synthesizer = (Synthesizer)sequencer;
                            channels = synthesizer.getChannels();
                        }
                        else {
                            synthesizer =
                                    MidiSystem.getSynthesizer();
                            channels = synthesizer.getChannels();
                        }
                        
                        sequencer.setSequence(MidiSystem.getSequence(f));
                        sequencer.open();
                        sequencer.start();
                        if (null != channels) {
                            for (MidiChannel channel : channels) {
                                channel.controlChange(7, 0);
                            }
                        }
                        
                        while(isPlaying) {
                            if(sequencer.isRunning()) {
                                try {
                                    Thread.sleep(100);
                                }
                                catch(InterruptedException ignore) {
                                    break;
                                }
                            }
                            else break;
                        }
                    }
                    
                }
                catch(MidiUnavailableException |
                        InvalidMidiDataException | IOException e) {
                    System.out.println(e.toString());
                }
                finally {
                    if (null != sequencer) {
                        sequencer.stop();
                        sequencer.close();
                    }
                }
            });	            
        }
    }
    public static void play(String file) {
            stop();
            p = new PlayingFile(file);	
            p.t1.start();
    }
    public static void stop() {
            if (null != p) {
                    p.isPlaying = false;
                    try {
                            p.t1.wait();
                    }
                    catch (Exception e) {
                            System.out.println(e);
                    }
                    p = null;
            }
    }
}
