package jogoutil;

import edu.princeton.cs.algs4.Queue;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Line.Info;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class PlayWave {
	private Clip clip=null;
	static String currentFile=null;
	static Queue<PlayWave> currentQ=new Queue<>();
        static PlayWave current = null;
	public PlayWave(File file) {		
	    try
	    {	    	
	        clip = (Clip)AudioSystem.getLine(new Info(Clip.class));
	        clip.addLineListener((LineEvent event) -> {
                    if (event.getType() == LineEvent.Type.STOP) {
                        clip.setFramePosition(0);
                    }
                });

	        clip.open(AudioSystem.getAudioInputStream(file));
	    }
	    catch (LineUnavailableException | UnsupportedAudioFileException 
                                                              | IOException exc)
	    {
	        exc.printStackTrace(System.out);
	    }		
	}
	
	public PlayWave(String fileName) {
		this(new File(fileName));
	}

	public void setGain(double value) {
            value = value / 100.0;
            try {
                FloatControl gainControl = 
                  (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                float dB = (float) 
                  (Math.log(value==0.0?0.0001:value)/Math.log(10.0)*20.0);
                gainControl.setValue(dB);
            } 
            catch (Exception ex) {
            }
	}
	
	
	public void loop() {
            setGain(10);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            clip.setFramePosition(0);
            clip.start();		
	}
	
	public void play() {
            try {
                clip.setFramePosition(0);
                clip.start();
            }
            catch (Exception e) {
                    e.printStackTrace(System.out);
            }
	}
	
	public void stop() {
            try {
                clip.stop();
            } 
            catch (Exception e) {
                    e.printStackTrace(System.out);
            }
	}
	
	static public void play(String file) {
            
            if (null == current || !currentFile.equals(file) || 
                                                 current.clip.isRunning()) {
                if (null != current) {
                    currentQ.enqueue(current);
                    if (currentQ.size()>10) currentQ.dequeue().stop();
                }
                currentFile = file;
                current = new PlayWave(new File(file));
            }		
            current.setGain(150);
            current.play();	
	}

}
